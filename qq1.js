var pageStart = 1;
var pageSize = 10;
var pageTotal;
var ypgjz;
var mgc;
var qx;
var qbsslx; //情报所属类型
var fzpzbs;//分值配置表数据
var fzmxs;//分值明细
var mrzt = 'font-family:FangSong_GB2312;font-size:16pt;line-height:1.5;word-wrap: break-word;'; //默认字体大小
var DWBH = "";
var bjqx = "";
var yhdw = "";
var qbbt = "";
var sfdwqb = false;
var len = "";
var SJCJ = "";
var fzbqnum = 0;
var gjz = "";
var userYPQX;

function xrFZPS(qb_bm, obj1, name, zjlx, str, bqmc, fzlx) {
  if (zjlx == "单选") {
    var float = false;
    for (var i = 0; i < qb_bm.length; i++) {
      var bqid = qb_bm[i].bmid + "_" + bqmc;
      qb_bm[i].check ? (float = true) : false;
      str += "<input type='radio' fzlx='" + fzlx + "' name=" + name + " onclick='pdFZLX(this)' " + (qb_bm[i].check ? "checked='checked'" : "") + " style='margin-left: 20px;' id=" + bqid + " value=" + qb_bm[i].bmmc + "  /><label for=" + bqid + ">" + qb_bm[i].bmmc + "</label>";
    }
    str += "<input type='radio' fzlx='" + fzlx + "' name=" + name + " onclick='pdFZLX(this)' " + (float ? "" : "checked='checked'") + " style='margin-left: 20px;' id=" + bqid + "_wu value='无' /><label for=" + bqid + "_wu>无</label>";
  } else {
    var float = false;
    for (var i = 0; i < qb_bm.length; i++) {
      var bqid = qb_bm[i].bmid + "_" + bqmc;
      str += "<input type='checkbox' fzlx='" + fzlx + "' name=" + name + " onclick='pdFZLX(this)' " + (qb_bm[i].check ? "checked='checked'" : "") + " style='margin-left: 20px;' id=" + bqid + " value=" + qb_bm[i].bmmc + "  /><label for=" + bqid + ">" + qb_bm[i].bmmc + "</label>";
    }
  }
  obj1.append(str);
}

$(document).ready(function () {
  $("#swflInfo").text(getBM('情报信息分类'));
  $("#shxxflInfo").text(getBM('社会信息分类'));
  //渲染情报标题，关注状态，权限显示
  cxQBCK(1);
  pdYPLX(fzqblb);//判断显示研判类型:涉稳分类,综合研判类信息
  //getQBCBDiv('bgn');
  getDW();
  $('#commitButton').click(function () {
    //获取选择的分值项
    var fzqbdj = $("input[name='qbdj']:checked").val(); //情报等级
    var fzqbdj1 = $("input[name='qbdj']:checked").next('label').html();
    var fzphaj = $("input[name='phaj']:checked").val(); //破获案件
    var fzphaj1 = $("input[name='phaj']:checked").next('label').html();
    var lwzx = $("input[name='lwzx']:checked").val(); //立为专线
    var lwzx1 = $("input[name='lwzx']:checked").next('label').html();
    var fzkfyy = $("input[name='kfyy']:checked").val(); //扣分原因
    var fzkfyy1 = $("input[name='kfyy']:checked").next('label').html();
    var fzqbfl = getSWFL();//下拉框选中的值
    var czcs = "";//处置措施
    if ($("#czcs_div").css('display') != 'none') {
      czcs = $("input[name='xxczcs']:checked").val() == undefined ? '' : $("input[name='xxczcs']:checked").val();
      if (qx == 'gly' && czcs == '') {
        alert('信息处置措施不能为空，请选择……');
        return false;
      }
    }

    var fznr = "{";
    for (var i = 0; i < fzbqnum; i++) {
      var name = 'fzbq' + i;
      var obj = document.getElementsByName(name);
      for (var j = 0; j < obj.length; j++) {
        if (obj[j].checked) {
          var id = obj[j].id;
          var key = id.substring(id.indexOf("_") + 1, id.length);
          if (obj[j].value != '无') {
            fznr += key + ":'" + obj[j].value + "',";
          }
        }
      }
    }
    fznr += "}";

    var bz = "";
    var rusult = 0;//综合研判类
    var rusult1 = "";
    var check_array = document.getElementsByName("zhyplck");
    for (var i = 0; i < check_array.length; i++) {
      if (check_array[i].checked == true) {
        rusult = parseInt(rusult) + parseInt(check_array[i].value);
        rusult1 = rusult1 + $("label[for='" + check_array[i].id + "']").html() + "@BZ@";
      }
    }
    if ("综合研判类信息" == fzqblb) {
      fzqbfl = "综合研判类信息";
      if (fzkfyy != undefined && fzkfyy != 0) {
        bz = bz + fzkfyy1;
      } else {
        bz = rusult1.substring(0, rusult1.lastIndexOf("@BZ@"));

      }
    } else {
      if (fzkfyy != undefined && fzkfyy != 0) {
        bz = bz + fzkfyy1;
      } else {
        bz = fzqbdj1 + "@BZ@" + fzphaj1 + "@BZ@" + lwzx1;
      }
    }
    //alert('情报等级'+fzqbdj+'---破获案件'+fzphaj+'---扣分原因'+fzkfyy+'----------综合研判'+rusult);
//              $('#commitButton').attr('disabled',"true");
    if (username == "null" || username == "未知用户") {
      alert("用户名已经丢失，请重新登录");
      $('#commitButton').removeAttr('disabled');
      return false;
    }

    var zw = "";
    if (qx == 'gly') {
      if (fzqbfl == '' || fzqbfl == '请选择') {
        alert('情报信息分类不能为空');
        return;
      }
    }
    if (getwb() == "" || getwb() == null || getwb() == undefined) {
      if (qx == 'gly') {
        zw = "收到";
      } else {
        alert('备注不能为空');
        return false;
      }
    } else {
      zw = getwb();
    }

    var xsfk_jzrq = "";
    if ($('#xsfk').is(':checked')) {
      var myDate = new Date();
      var sjjg = $('#xsfk_txt').val();
      myDate.setDate(myDate.getDate() + parseInt(sjjg));
      xsfk_jzrq = myDate.getFullYear() + "-" + (myDate.getMonth() + 1) + "-" + myDate.getDate() + " 23:59:59";
    }
    var zyqb = document.getElementById('zyqb').checked;
    var data = {'uuid': uuid, 'username': username, 'zw': getwb()};
    if (sfbjqb) {
      $('#fbhf').css('display', 'none');
      BJ_qbrn(data);
    } else {
      if (confirm('你确定要回复情报吗？')) {
        ShowLoadingDiv("正在回复，请等待……");
        var ypuuid = getUUID();
        var fzmxuuid = getUUID();
        $.ajax({
          url: basePath + 'servlet/back/QBBS2S?type=YPQB',
          type: 'POST',
          timeout: 1000 * 60 * 60,
          data: {
            FZYPTJ: fznr,
            'CZCS': czcs,
            'cblx': 'ypqb',
            xsfk_sj: xsfk_jzrq,
            'username': username,
            'uuid': uuid,
            'ypuuid': ypuuid,
            'ZYQB': zyqb,
            'zw': zw,
            'swfl': getSWFL(),
            'qbdj': getQBDJ(),
            'qx': qx,
            yhdw: yhdw,
            'fzmxuuid': fzmxuuid,
            'fzqbfl': fzqbfl,
            'bz': bz,
            'fzqbdj': fzqbdj,
            'fzphaj': fzphaj,
            'rusult': rusult,
            'fzkfyy': fzkfyy,
            'lwzx': lwzx
          },
          dataType: 'json',
          success: function (obj, textStatus) {       //obj='{data:[{KHID:'',KHMC:'',XMLJ:'',ITAPSSLJ:'',CPKJ:'',SJKLJ:'',BKJ:'',ZXSJGC:'',LOCK:'',SYSX:'',SJ_GX:'',ITAPSS_GX:'',SJSJ:'',ZFBC:''}]}'
            if (obj.success) {
              $('#commitButton').removeAttr('disabled');
              $('#fileupload').ajaxSubmit({
                url: basePath + 'servlet/back/QBBS2S?type=addFJ&ypuuid=' + ypuuid + '&username=' + username + '&QBID=' + uuid + '&QBBT=' + qbbt,
                type: 'post',
                timeout: 1000 * 60 * 60,
                data: {},
                dataType: 'json',
                success: function (obj, textStatus) {
                  if (obj.success) {
                    //alert("上报成功");
                    if (qx == 'gly') {
                      try {
                        window.opener.windowOnloads();
                      } catch (e) {
                      }
                      window.close();
                    } else {
                      $("#resetButton").trigger("click");
                      pageTotal++;
                      cxQBCK(0);
                      pageStart = Math.ceil(pageTotal / pageSize);
                      Page(pageStart);
                    }
//                                                                              window.location.href=window.location.href; 
//                                                                              window.location.reload;
//                                                                              editor.html('');
//                                                                              $('#editor_id_text').val('');
                    ue.setContent('');
                    HideLoadingDiv();
                  } else {
                    alert("服务器繁忙,请稍候再试...");
                  }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                  alert(errorThrown);
                }
              });
            } else {
              alert(obj.msg);
            }
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
          }
        });
      } else {
        $('#commitButton').removeAttr('disabled');
      }
    }

  });
});